{ lib, stdenv, fetchurl, ... }:

let
  name = "dnclient";
  version = "v0.5.2";
  commit = "22c2c91a";
in stdenv.mkDerivation {
  inherit name version;
  
  # This source seems linked to a commit version so it should be stable.
  # It is closed-source, however.
  src = fetchurl {
    url = "https://dl.defined.net/${commit}/${version}/linux/amd64/dnclient";
    sha256 = "sha256-IYX1khOj3TNIijKPc77XeD8eJCjFIOdLsZONTGHZ7a0=";
  };

  phases = [ "installPhase" ];

  installPhase = ''
    mkdir -p $out/bin
    cp $src $out/bin/dnclient
    chmod +x $out/bin/dnclient
  '';

  meta = with lib; {
    description = "Defined Networks' dnclient application";
    homepage = "https://www.defined.net";
    # this is available for other platforms...but I don't want to figure that out now
    platforms = platforms.unix;
  };
}
